<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('elitepro.sitename', 'Compwhiz') }}</title>

    <!--google material design icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!--materializecss css-->
    <link href="{{ asset('css/materialize.min.css') }}" type="text/css" rel="stylesheet" />
    <!--Syntax Highlighter prism css-->
    <link href="{{ asset('plugins/prism/prism.css') }}" type="text/css" rel="stylesheet" />
    <!--Animate css-->
    <link href="{{ asset('plugins/animate.css/animate.min.css') }}" type="text/css" rel="stylesheet" />
    <!--sweetalert-->
    <link href="{{ asset('plugins/sweetalert/dist/sweetalert.css') }}" type="text/css" rel="stylesheet" />
    <!--owlcarousel2-->
    <link rel="stylesheet" href="{{ asset('plugins/OwlCarousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/OwlCarousel/dist/assets/owl.theme.default.min.css') }}">
    <!--chartist-->
    <link href="{{ asset('plugins/chartist/dist/chartist.min.css') }}" type="text/css" rel="stylesheet" />
    <!--morris.js-->
    <link href="{{ asset('plugins/morris.js/morris.css') }}" type="text/css" rel="stylesheet" />
    <!--nvd3 chart-->
    <link href="{{ asset('plugins/nvd3/build/nv.d3.min.css') }}" type="text/css" rel="stylesheet" />
    <!-- quill text editor -->
    <link href="{{ asset('plugins/quill/dist/quill.snow.css') }}" rel="stylesheet">
    <!--data tables-->
    <link href="{{ asset('plugins/DataTables/media/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <!--Custom css-->
    <link href="{{ asset('css/app-style.css') }}" type="text/css" rel="stylesheet" />
    <!--page theme css-->
    <link href="{{ asset('themes/app-theme.css') }}" type="text/css" rel="stylesheet" />
</head>
<body>
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- pre page loader-->
<div id="pre-page-loader">
    <div id="pre-page-loader-center">
        <div id="pre-page-loader-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
            <div class="object" id="object_big"></div>
        </div>
    </div>
</div>
<!--End pre page loader-->
<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- //////////////////////////////////////////////////////////////////////////// -->
<!--Navbar-->
<nav id="me-navbar" class="white">
    <div class="nav-wrapper">
        <a id="logo-container" href="dashboard.html" class="brand-logo">EliteProWriters</a>
        <div class="row m-l-sidbar m-b-t-no">
            <div class="col l3 m6 s6" id="me-left-navbar">
                <ul class="left">
                    <li><a href="#" data-activates="left-sidebar" class="waves-effect waves-default sidebar-collapse hide-on-large-only"><i class="material-icons">menu</i></a></li>
                    <li id="notifications-box"><a class='dropdown-button waves-effect waves-default' href='#' data-activates='notifications-dropdown'><i class="material-icons">notifications</i><span id="notification-cout">3</span></a></li>
                </ul>
                <ul id='notifications-dropdown' class='dropdown-content'>
                    <li>
                        <a href="#!">
                            <i class="material-icons">email</i>
                            <p class="noti-message">New mail from jhon<span>15 minutes ago</span></p>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#!">
                            <i class="material-icons">chat</i>
                            <p class="noti-message">New member John joined<span>1 hour ago</span></p>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#!">
                            <i class="material-icons">perm_identity</i>
                            <p class="noti-message">New mail from jhon<span>15 minutes ago</span></p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col l6 hide-on-med-and-down" id="me-desktop-search">
                <div class="col l12">
                    <form class="desktop-search-box">
                        <div class="input-field desktop-search-div">
                            <input id="desktop-search" type="search" placeholder="Search What You Want?" required>
                            <label for="desktop-search"><i class="material-icons">search</i></label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col m12 s12 hide-on-large-only" id="me-mobile-search">
                <div class="col m12 s12">
                    <form class="mobile-search-box">
                        <div class="input-field mobile-search-div">
                            <input id="mobile-search" type="search" placeholder="Search What You Want?" required>
                            <label for="mobile-search"><i class="material-icons  waves-effect waves-default" id="me-hide-mobile-search">keyboard_backspace</i></label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col l3 m6 s6" id="me-right-navbar">
                <ul class="right">
                    <li class="hide-on-large-only"><a id="me-show-mobile-search" class="waves-effect waves-default"><i class="material-icons">search</i></a></li>
                    <li id="user-account-box"><a class='dropdown-button waves-effect waves-default' href='#' data-activates='user-account-dropdown'><i class="material-icons">account_circle</i></a></li>
                    <li class="hide-on-small-only"><a id="me-pageRefresh" class="waves-effect waves-default"><i class="material-icons">refresh</i></a></li>
                    <li class="hide-on-small-only"><a id="me-fullscreen" class="waves-effect waves-default"><i class="material-icons">fullscreen</i></a></li>
                </ul>
                <ul id='user-account-dropdown' class='dropdown-content'>
                    <li><a href="#!"><i class="material-icons">perm_identity</i>My Profile</a></li>
                    <li><a href="#!"><i class="material-icons">settings</i>Page Customizer</a></li>
                    <li><a href="#!"><i class="material-icons">power_settings_new</i>Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!--End navbar-->
<!-- //////////////////////////////////////////////////////////////////////////// -->


<!-- //////////////////////////////////////////////////////////////////////////// -->
<!--Left sidebar-->
<aside id="left-sidebar-nav">
    <div id="left-sidebar" class="side-nav fixed">
        <ul class="leftside-navigation">
            <li class="navigation">Navigation</li>
            <li><a href="dashboard.html" class="waves-effect waves-default"><i class="material-icons left-icon">dashboard</i>Dashboard</a></li>

        </ul>
        <div class="fixed sidebar-footer">
            <ul>
                <li>
                    <a class="tooltipped" data-delay="0" data-position="top" data-tooltip="Feedback">
                        <i class="material-icons"> feedback </i>
                    </a>
                </li>
                <li>
                    <a class="tooltipped" data-delay="0" data-position="top" data-tooltip="Help">
                        <i class="material-icons"> help </i>
                    </a>
                </li>
                <li>
                    <a class="tooltipped" data-delay="0" data-position="top" data-tooltip="API">
                        <i class="material-icons"> code </i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>
<!--End left sidebar-->
<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- //////////////////////////////////////////////////////////////////////////// -->
<!--Page Body-->
<main>
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!--Page body title-->
    <div class="me-page-title">
        <!--Page Title-->
        <h1>Page Title</h1>
        <!--Page description-->
        <p>Page Descrition</p>
    </div>
    <!--End page body title-->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!--Page body content-->
    <div class="me-page-body">
        <div class="row">
            <div class="col s12">
                <h2 class="header">Title</h2>
                <p>Title descrition</p>
            </div>
        </div>
    </div>
    <!--End page body content-->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
</main>
<!--End page body-->
<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- //////////////////////////////////////////////////////////////////////////// -->
<!--Footer-->
<footer class="page-footer white" id="me-footer">
    <div class="footer-copyright">
        <div class="left copyright-text">
            &copy; EliteProWriters
        </div>
        <div class="right resources-box">
            <ul>
                <li><a href="#">Terms of Ser.</a></li>
                <li><a href="#">SLA</a></li>
                <li><a href="#">Pri. &amp; terms</a></li>
                <li id="scroll-top-dash"><i class="material-icons">arrow_upward</i></li>
            </ul>
        </div>
    </div>
</footer>
<!--End footer-->
<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- //////////////////////////////////////////////////////////////////////////// -->
<!--  Scripts-->

<!-- Jquery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Materializecss js -->
<script src="{{ asset('js/materialize.min.js') }}"></script>
<!--Chartjs -->
<script src="{{ asset('plugins/chartjs/dist/Chart.min.js') }}"></script>
<!--Syntax Highlighter prism js-->
<script src="{{ asset('plugins/prism/prism.js') }}"></script>
<!--Sweetalert-->
<script src="{{ asset('plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
<!--chartist-->
<script src="{{ asset('plugins/chartist/dist/chartist.min.js') }}"></script>
<!--morris chart-->
<script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('plugins/morris.js/raphael.min.js') }}"></script>
<!--sparkline chart-->
<script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!--nvd3 and d3 chart-->
<script src="{{ asset('plugins/nvd3/d3/d3.min.js') }}"></script>
<script src="{{ asset('plugins/nvd3/build/nv.d3.min.js') }}"></script>
<!--owlcarousel2-->
<script src="{{ asset('plugins/OwlCarousel/dist/owl.carousel.min.js') }}"></script>
<!--jQuery Validation-->
<script src="{{ asset('plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<!--Jquery formatter.js-->
<script type="text/javascript" src="{{ asset('plugins/formatter.js/dist/jquery.formatter.min.js') }}"></script>
<!-- quill text editor -->
<script src="{{ asset('plugins/quill/dist/quill.min.js') }}"></script>
<!-- data table -->
<script type="text/javascript" src="{{ asset('plugins/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Custom Js -->
<script src="{{ asset('js/init.js') }}"></script>
</body>
</html>
